# Fob Solutions test task

This is the test task, solved by Pavel Shilov. 
Please, read this topic before start using it to know how to run application.

## Technology stack: 

- Java (+ Spring)
- Angular (+ Typescript, HTML, SCSS)
- JHipster as the generator of the sample Java Spring + Angular application

## Why I have used JHipster? 

- Heard about it a lot, but never used before. As the requirements sounds: Java + Angular + Docker, I have decided that now I have a   chance to try it.

# How to run

    First of all you will need to install following dependencies: 
        - Java, Maven
        - NodeJS + npm

    1. Clone this repository 
    2. From the root of the application run: 
        mvnw (for Windows)
        ./mvnw (For linux)
    3. Go to http://localhost:8080
    4. Follow Instructions there.

## DockerFile
If you need a Docker File, you will find it on: 
    src/main/docker/

# Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1.  [Node.js][]: 

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

    npm install

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./mvnw (mvnw for Windows)
    npm start

Npm is also used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in [package.json](package.json). You can also run `npm update` and `npm install` to manage dependencies.
Add the `help` flag on any command to see how you can use it. For example, `npm help update`.

The `npm run` command will list all of the scripts available to run for this project.

## Docker

    Before using following, do not forget to install: 
        - Docker
        - Docker-compose

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    ./mvnw package -Pprod verify jib:dockerBuild

Then run:

    docker-compose -f src/main/docker/app.yml up -d

### Managing dependencies

For example, to add [Leaflet][] library as a runtime dependency of your application, you would run following command:

    npm install --save --save-exact leaflet

To benefit from TypeScript type definitions from [DefinitelyTyped][] repository in development, you would run following command:

    npm install --save-dev --save-exact @types/leaflet

Then you would import the JS and CSS files specified in library's installation instructions so that [Webpack][] knows about them:
Edit [src/main/webapp/app/vendor.ts](src/main/webapp/app/vendor.ts) file:

```
import 'leaflet/dist/leaflet.js';
```

Edit [src/main/webapp/content/css/vendor.css](src/main/webapp/content/css/vendor.css) file:

```
@import '~leaflet/dist/leaflet.css';
```

Note: there are still few other things remaining to do for Leaflet that we won't detail here.

For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].

### Using angular-cli

You can also use [Angular CLI][] to generate some custom client code.

For example, the following command:

    ng generate component my-component

will generate few files:

    create src/main/webapp/app/my-component/my-component.component.html
    create src/main/webapp/app/my-component/my-component.component.ts
    update src/main/webapp/app/app.module.ts

## Building for production

To optimize the fobsolutions application for production, run:

    ./mvnw -Pprod clean package

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

    java -jar target/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.
