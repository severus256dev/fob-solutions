package com.fobsolutions.stackcontrol.service;

import org.springframework.stereotype.Service;

import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class StackService {

    Stack<Integer> stack = new Stack<Integer>();

    public Stack<Integer> initStack() {
        this.stack.clear();
        for (int i = 0; i < ThreadLocalRandom.current().nextInt(5, 20); i++) {
            this.stack.push(ThreadLocalRandom.current().nextInt(i, i + 17));
        }
        return this.stack;
    }

    public Stack<Integer> getStack() {
        return this.stack;
    }

    public Stack<Integer> push(Integer value) {
        this.stack.push(value);
        return this.stack;
    }

    public Stack<Integer> pop() {
        if (!this.stack.empty()) this.stack.pop();

        return this.stack;
    }
}
