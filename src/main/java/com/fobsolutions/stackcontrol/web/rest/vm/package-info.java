/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fobsolutions.stackcontrol.web.rest.vm;
