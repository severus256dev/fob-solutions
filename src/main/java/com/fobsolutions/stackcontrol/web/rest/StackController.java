package com.fobsolutions.stackcontrol.web.rest;

import com.fobsolutions.stackcontrol.service.StackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Stack;

@RestController
@RequestMapping("/api/stack")
public class StackController {

    @Autowired
    private StackService stackService;

    @GetMapping("/init")
    Stack<Integer> initStack() {
        return stackService.initStack();
    }

    @GetMapping("/view")
    Stack<Integer> getStack() {
        return stackService.getStack();
    }

    @PostMapping("/push") Stack<Integer> push(@RequestParam(name = "value", required = true) Integer value) {
        return stackService.push(value);
    }

    @GetMapping("/pop") Stack<Integer> pop() {
        return stackService.pop();
    }
}
