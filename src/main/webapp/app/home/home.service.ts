import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as constants from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  initStack() {
    return this.http.get<any[]>(constants.SERVER_API_URL + '/api/stack/init');
  }

  popFromStack() {
    return this.http.get<any[]>(constants.SERVER_API_URL + '/api/stack/pop');
  }

  pushToStack(value: string) {
    return this.http.post<any[]>(constants.SERVER_API_URL + '/api/stack/push?value=' + value, {});
  }

}
