import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
    stack: any[];
    valueToPush: string;

    regexNumber = '^[0-9]*$';

    constructor(private _service: HomeService) { }

    ngOnInit() {
    }

    initStack() {
        this._service.initStack().subscribe((data => {
            this.stack = data;
        }));
    }

    popFromStack() {
        this._service.popFromStack().subscribe((data => {
            this.stack = data;
        }));
    }

    pushToStack() {
        this._service.pushToStack(this.valueToPush).subscribe((data => {
            this.stack = data;
        }));
        this.valueToPush = undefined;
    }

    isNotCorrectValue() {
        if (!this.valueToPush.match(this.regexNumber)) {
            return true;
        }
        return false;
    }
}
